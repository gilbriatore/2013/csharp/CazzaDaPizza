﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Services;
using Dominios;

namespace Telas
{
    public partial class TelaCliente : Form
    {
        private int contador;
        StreamWriter sw = new StreamWriter(@"ciclo_de_vida.txt");

       /* using System.IO;
        StreamWriter sw = new StreamWriter(@"ciclo_de_vida.txt");
        sw.WriteLine("Evento {0} ocorrido no construtor", ++contador);
        sw.Dispose(); */
        

        private CadastroDeCliente cadastroCliente;

        public TelaCliente()
        {
            InitializeComponent();
            cadastroCliente = new CadastroDeCliente();
           // this.Controls.Add(new Button());
           // this.Controls.Add(new TextBox());

            sw.WriteLine("Evento {0} ocorrido no construtor", ++contador);
        }

        private void TelaCliente_Load(object sender, EventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Load", ++contador);
        }

        private void TelaCliente_FormClosed(object sender, FormClosedEventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_FormClosed", ++contador);
        }

        private void TelaCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_FormClosing", ++contador);
        }

        private void TelaCliente_Shown(object sender, EventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Shown", ++contador);
        }

        private void TelaCliente_Activated(object sender, EventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Activated", ++contador);
        }

        private void TelaCliente_Deactivate(object sender, EventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Deactivate", ++contador);
        }

        private void TelaCliente_Layout(object sender, LayoutEventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Layout", ++contador);
        }

        private void TelaCliente_Move(object sender, EventArgs e)
        {
            sw.WriteLine("Evento {0} ocorrido no TelaCliente_Move", ++contador);
        }
    }
}
