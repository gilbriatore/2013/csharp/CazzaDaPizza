﻿namespace Telas
{
    partial class TelaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            sw.WriteLine("Evento {0} ocorrido no Dispose", ++contador);
            sw.Dispose();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TelaCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 415);
            this.Name = "TelaCliente";
            this.Text = "Form1";
            this.Activated += new System.EventHandler(this.TelaCliente_Activated);
            this.Deactivate += new System.EventHandler(this.TelaCliente_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TelaCliente_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TelaCliente_FormClosed);
            this.Load += new System.EventHandler(this.TelaCliente_Load);
            this.Shown += new System.EventHandler(this.TelaCliente_Shown);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.TelaCliente_Layout);
            this.Move += new System.EventHandler(this.TelaCliente_Move);
            this.ResumeLayout(false);

        }

        #endregion
    }
}

